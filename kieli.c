#include <stdio.h>
#include "kieli.h"

void tulostaMoiMaailma(const char *kieli) {
    if (strcmp(kieli, "englanti") == 0) {
        printf("Hello, World!\n");
    } else if (strcmp(kieli, "ruotsi") == 0) {
        printf("Hej världen!\n");
    } else {
        printf("Kieltä ei tueta!\n");
    }
}

  GNU nano 7.2                         main.c
#include <stdio.h>
#include "kieli.h"

int main() {
    // Tulosta "Hello, World!" englanniksi
    tulostaMoiMaailma("englanti");

    // Tulosta "Hello, World!" suomeksi
    tulostaMoiMaailma("suomi");

    // Tulosta "Hello, World!" ruotsiksi
    tulostaMoiMaailma("ruotsi");

    return 0;
}


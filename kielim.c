
#include <stdio.h>
#include "kieli.h"

int main() {
    // Tulosta "Hello, World!" englanniksi
    printHelloWorld("English");

    // Tulosta "Hello, World!" ruotsiksi
    printHelloWorld("Swedish");

    // Tulosta "Hello, World!" suomeksi
    printHelloWorld("Finnish");

    return 0;
}
